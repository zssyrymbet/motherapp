//
//  MainPageViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    var articles = [Article]()
    var filteredData = [CategoryCard]()
    var categories = [CategoryCard]()
    var categoryArticles = [Article]()
    var cellValue: Category?
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        search.delegate = self
        search.enablesReturnKeyAutomatically = false
        filteredData = categories
        
        if Singleton.shared.userData != nil {
            APIManagerCategories.getCategoriesById(id: cellValue!.id) { categoryList in
                self.categories = categoryList
                self.collectionView.reloadData()
                for category in categoryList {
                    APIManagerDict.getArticlesByCategoryId(id: category.id) { articleList in
                        self.categoryArticles.append(contentsOf: articleList)
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}

extension MainPageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleTableViewCell
        let articleCell = categoryArticles[indexPath.row]
        cell.article = articleCell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ArticleDetailViewController") {
            if let viewController = segue.destination as? ArticleDetailViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = categoryArticles[index]
                viewController.type = cellValue?.type
            }
        }
    }
}

extension MainPageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCardsCollectionViewCell
            if isSearching {
                cell.category = filteredData[indexPath.row]
            } else {
                let categoryCell = categories[indexPath.row]
                cell.category = categoryCell
            }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = categories[indexPath.item].id
        APIManagerDict.getArticlesByCategoryId(id: id) { [self] article in
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! ArticleDetailViewController 
            viewController.cellValue = article[0]
            viewController.type = cellValue?.type
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            collectionView.reloadData()
        } else {
            isSearching = true
            filteredData = categories.filter({$0.topic.lowercased().contains(searchBar.text!.lowercased() )})
            collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
}
