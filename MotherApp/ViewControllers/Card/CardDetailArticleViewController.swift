//
//  CardDetailArticleViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 26.03.2021.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CardDetailArticleViewController: UIViewController {

    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    var cellValue: CategoryCard?
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var saveToFav: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = cellValue?.topic
        
        APIManagerDict.getArticlesByCategoryId(id: cellValue!.id) { [self] article in
            self.articleTitle.text = article[0].topic ?? ""
            self.textView.text = article[0].text.toTtml
            icon.sentIcons(from: cellValue!.iconUrl)
        }
    }
    
    @IBAction func saveToFavButton(_ sender: Any) {
        APIManagerDict.saveToFavourites(articleId: (cellValue?.id)!) { response in
            if response == "success" {
                self.likeIcon.tintColor = UIColor.red
                self.saveToFav.textColor = UIColor.red
                self.saveToFav.text = "Удалить из избранного"
            }
        }
    }
    
    @IBAction func deleteFromFav(_ sender: Any) {
        APIManagerDict.deleteFromFavourites(articleId: (cellValue?.id)!) { response in
            if response == "success" {
                self.likeIcon.tintColor = UIColor(red: 0.82, green: 0.63, blue: 0.68, alpha: 1.0)
                self.saveToFav.textColor = UIColor(red: 0.82, green: 0.63, blue: 0.68, alpha: 1.0)
                self.saveToFav.text = "Добавить в избранное"
            }
        }
    }
    
    @IBAction func getFavList(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "favouriteVC") as! FavouriteListViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
}

extension Data {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var toTtml: String { attributedString?.string ?? "" }
}

extension StringProtocol {
    var attributedString: NSAttributedString? {
        Data(utf8).attributedString
    }
    var toTtml: String {
        attributedString?.string ?? ""
    }
}

extension UIImageView {
    func sentIcons(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func sentIcons(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

