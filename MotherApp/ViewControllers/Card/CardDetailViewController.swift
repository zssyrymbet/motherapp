//
//  CardDetailViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 25.03.2021.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CardDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var cellValue: Category?
    var categories = [CategoryCard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = cellValue?.name
        
        APIManagerCategories.getCategoriesById(id: cellValue!.id) { [self] categoryList in
            self.categories = categoryList
            self.tableView.reloadData()
        }
    }
}

extension CardDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCardsTableViewCell
            let categoryCell = categories[indexPath.row]
            cell.category = categoryCell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CardDetailArticleViewController") {
            if let viewController = segue.destination as? CardDetailArticleViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = categories[index]
            }
        }
    }
}

