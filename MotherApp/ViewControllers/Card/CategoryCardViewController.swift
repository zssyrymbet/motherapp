//
//  CategoryCardViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CategoryCardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var search: UISearchBar!
    var categories = [Category]()
    var categoriesCard = [Category]()
    var filteredData: [Category]!
    var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        search.delegate = self
        search.enablesReturnKeyAutomatically = false
        filteredData = categories

        if Singleton.shared.user != nil {
            APIManagerCategories.getCategoriesCard { categoryList in
                self.categoriesCard = categoryList
                self.tableView.reloadData()
            }
            
            APIManagerCategories.getCategories { categoryList in
               self.categories = categoryList
               self.collectionView.reloadData()
            }
        }
    }
}

extension CategoryCardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCategory", for: indexPath) as! CardCategoryTableViewCell
        cell.configureCardCategoryCell(category: categoriesCard[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CardDetailViewController") {
            if let viewController = segue.destination as? CardDetailViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = categoriesCard[index]
            }
        }
    }
}

extension CategoryCardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CardsCollectionViewCell
        if isSearching {
            cell.category = filteredData[indexPath.row]
        } else {
            let categoryCell = categories[indexPath.row]
            cell.category = categoryCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainPageViewController
        if let indexPath = collectionView.indexPathsForSelectedItems?.first {
            viewController.cellValue = categories[indexPath.row]
        }
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            collectionView.reloadData()
        } else {
            isSearching = true
            filteredData = categories.filter({$0.name.lowercased().contains(searchBar.text!.lowercased() )})
            collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
}

