//
//  EventListViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 13.05.2021.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

protocol UpdateEventVCDelegate: class {
    func didTapClose()
}

class EventListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var eventDate: UITextField!
    @IBOutlet weak var eventTime: UITextField!
    @IBOutlet weak var notifyDate: UITextField!
    @IBOutlet weak var notifyTime: UITextField!
    @IBOutlet weak var notes: UITextField!
    weak var eventDelegate: UpdateEventVCDelegate?
    var cellValue: Event?
    var events: [Event]?
    var eventList: [Event] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        
        if cellValue != nil {
            eventList = events!
            eventName.text = cellValue?.name
            eventTime.text = cellValue?.time
            notifyTime.text = cellValue?.remindAtTime
            notes.text = cellValue?.notes
            self.title = cellValue?.name
            
            let inputFormatter = DateFormatter()
            inputFormatter.locale = Locale(identifier: "en_US_POSIX")
            inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let showDate = inputFormatter.date(from: (cellValue?.date)!)
            let remindDate = inputFormatter.date(from: (cellValue?.remindAtDate)!)
            inputFormatter.dateFormat = "dd.MM.yyyy"
            eventDate.text = inputFormatter.string(from: showDate!)
            notifyDate.text = inputFormatter.string(from: remindDate!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventList = events!
    }
    
    @IBAction func changeEvent(_ sender: Any) {
        let inputFormatter = DateFormatter()
        inputFormatter.locale = Locale(identifier: "en_US_POSIX")
        inputFormatter.dateFormat = "hh:mm:ss a"
        let showTime = inputFormatter.date(from: (eventTime.text)!)
        let remindTime = inputFormatter.date(from: (notifyTime.text)!)
        inputFormatter.dateFormat = "hh:mm:ss"
        APIManagerEvent.updateEvent(id: (cellValue?.id)!, date: eventDate.text!, name: eventName.text!, notes: notes.text!, remindAtDate: notifyDate.text!, remindAtTime: inputFormatter.string(from: remindTime!), time: inputFormatter.string(from: showTime!)) { response in
            if response == "success" {
                self.eventDelegate?.didTapClose()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension EventListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let text = eventList[indexPath.row].notes
        let height = text!.height(withConstrainedWidth: 289, font: UIFont.systemFont(ofSize: 15))
        return CGFloat(100 + height)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let text = eventList[indexPath.row].notes
        let height = text!.height(withConstrainedWidth: 289, font: UIFont.systemFont(ofSize: 15))
        return CGFloat(100 + height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
        cell.configureNotificationCell(event: eventList[indexPath.row])
        return cell
    }
}
