//
//  NotificationViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import EventKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notesText: UITextView!
    var eventList: [Event] = []
    var selectdeIndex = -1
    var isCollapse = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        
        if Singleton.shared.userData != nil {
            APIManagerEvent.getEvents { events in
                self.eventList = events
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Singleton.shared.userData != nil {
            APIManagerEvent.getEvents { events in
                self.eventList = events
                self.tableView.reloadData()
            }
        }
    }
    
    private func setupNavBar() {
        self.navigationItem.title = "Уведомление"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectdeIndex == indexPath.row && isCollapse == true {
            self.notesText.text = eventList[indexPath.row].notes
            self.notesText.isHidden = true
            
            let text = eventList[indexPath.row].notes
            let height = text!.height(withConstrainedWidth: 289, font: UIFont.systemFont(ofSize: 15))
            return CGFloat(100 + height)
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
        cell.configureNotificationCell(event: eventList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if selectdeIndex == indexPath.row {
            if self.isCollapse == false {
                self.isCollapse = true
            } else {
                self.isCollapse = false
            }
        } else {
            self.isCollapse = true
        }
        
        self.selectdeIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}



