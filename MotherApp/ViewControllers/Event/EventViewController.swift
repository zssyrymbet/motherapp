//
//  EventViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/10/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
import FSCalendar
import EventKit

class EventViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendarTitle: UILabel!
    @IBOutlet weak var calendarView: UICollectionView!
    
    var eventList: [Event] = []
    var events: [Event] = []
    let months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октярь", "Ноябрь", "Декабрь"]
    let monthEng = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    let daysOfMonth = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31]
    var currentMonth = String()
    var currentMonthEng = String()
    var numberOfEmptyBox = Int()
    var nextNumberOfEmptyBox = Int()
    var previousNumberOfEmptyBox = 0
    var direction = 0
    var positionIndex = 0
    var leapYearCounter = 2
    var dayCounter = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        currentMonth = months[month]
        currentMonthEng = monthEng[month]
        calendarTitle.text = "\(currentMonth) \(year)"
        if weekday == 0 {
            weekday = 7
        }
        getStartDateDayPosition()
        didTapClose()
        
        
        if Singleton.shared.userData != nil {
            APIManagerEvent.getEvents { events in
                self.eventList = events
                self.tableView.reloadData()
            }
        }
    }
    
    func getStartDateDayPosition() {
        switch direction {
        case 0:
            numberOfEmptyBox = weekday
            dayCounter = day
            while dayCounter>0 {
                numberOfEmptyBox = numberOfEmptyBox - 1
                dayCounter = dayCounter - 1
                if numberOfEmptyBox == 0 {
                    numberOfEmptyBox = 7
                }
            }
            if numberOfEmptyBox == 7 {
                numberOfEmptyBox = 0
            }
            positionIndex = numberOfEmptyBox
        case 1...:
            nextNumberOfEmptyBox = (positionIndex + daysInMonth[month])%7
            positionIndex = nextNumberOfEmptyBox
            
        case -1:
            previousNumberOfEmptyBox = (7 - (daysInMonth[month] - positionIndex)%7)
            if previousNumberOfEmptyBox == 7 {
                previousNumberOfEmptyBox = 0
            }
            positionIndex = previousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
    
    @IBAction func addEventButton(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let viewController = storyboard.instantiateViewController(withIdentifier: "createEvent") as! CreateEventViewController
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(viewController, animated: true, completion: nil)
            viewController.delegate = self
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        self.tableView.isEditing = !self.tableView.isEditing
    }
    
    @IBAction func backCalendarButton(_ sender: Any) {
        switch currentMonth {
        case "January":
            direction = -1
            month = 11
            year -= 1
            
            if leapYearCounter > 0 {
                leapYearCounter -= 1
            }
            
            if leapYearCounter == 0 {
                daysInMonth[1] = 29
                leapYearCounter = 4
            } else {
                daysInMonth[1] = 28
            }
            
            getStartDateDayPosition()
            currentMonth = months[month]
            currentMonthEng = monthEng[month]
            calendarTitle.text = "\(currentMonth) \(year)"
            calendarView.reloadData()
            
        default:
            direction = -1
            month -= 1
            getStartDateDayPosition()
            currentMonth = months[month]
            currentMonthEng = monthEng[month]
            calendarTitle.text = "\(currentMonth) \(year)"
            calendarView.reloadData()
        }
    }
    
    @IBAction func nextCalendarButton(_ sender: Any) {
        switch currentMonth {
        case "December":
            direction = 1
            month = 0
            year += 1
            
            if leapYearCounter  < 5 {
                leapYearCounter += 1
            }
            
            if leapYearCounter == 4 {
                daysInMonth[1] = 29
            }
            
            if leapYearCounter == 5 {
                leapYearCounter = 1
                daysInMonth[1] = 28
            }
            getStartDateDayPosition()
            currentMonth = months[month]
            currentMonthEng = monthEng[month]
            calendarTitle.text = "\(currentMonth) \(year)"
            calendarView.reloadData()
        default:
            direction = 1
            getStartDateDayPosition()
            month += 1
            currentMonth = months[month]
            currentMonthEng = monthEng[month]
            calendarTitle.text = "\(currentMonth) \(year)"
            calendarView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch direction {
        case 0:
            return daysInMonth[month] + numberOfEmptyBox
        case 1...:
            return daysInMonth[month] + nextNumberOfEmptyBox
        case -1:
            return daysInMonth[month] + previousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCollectionViewCell
        cell.backgroundColor = UIColor.clear
        cell.calendarDate.textColor = UIColor.black
        cell.Circle.isHidden = true
        
        if cell.isHidden {
            cell.isHidden = false
        }
        
        switch direction {
            case 0:
                cell.calendarDate.text = "\(indexPath.row + 1 - numberOfEmptyBox)"
            case 1:
                cell.calendarDate.text = "\(indexPath.row + 1 - nextNumberOfEmptyBox)"
            case -1:
                cell.calendarDate.text = "\(indexPath.row + 1 - previousNumberOfEmptyBox)"
            default:
                fatalError()
        }
        
        if Int(cell.calendarDate.text!)! < 1 {
            cell.isHidden = true
        }
        
        switch indexPath.row {
            case 5,6,12,13,19,20,26,27,33,34:
                print("indexPath.row  ", indexPath.row )
                if Int(cell.calendarDate.text!)! > 0 {
                    cell.calendarDate.textColor = UIColor(red: 0.29, green: 0.1, blue: 0.60, alpha: 1.0)
                }
            default:
                break
        }
        
        if currentMonth == months[calendar.component(.month, from: date) - 1] && year == calendar.component(.year, from: date) && indexPath.row + 1 - numberOfEmptyBox == day {
            cell.Circle.isHidden = false
            cell.DrawCircle()
        }
                
        for event in eventList {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let date = dateFormatter.date(from: event.date!)
            dateFormatter.dateFormat = "MMMM d yyyy"
            let dateString = dateFormatter.string(from: date ?? Date())
            let calendarDay = "\(currentMonthEng) \(indexPath.row - 2) \(year)"
            
            if (dateString == calendarDay) {
                cell.Circle.isHidden = false
                cell.DrawEventCircle()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "editEvent") as! EventListViewController
        
        for event in eventList {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let date = dateFormatter.date(from: event.date!)
            dateFormatter.dateFormat = "MMMM d yyyy"
            let dateString = dateFormatter.string(from: date ?? Date())
            let calendarDay = "\(currentMonthEng) \(indexPath.item - 2) \(year)"

            if (dateString == calendarDay) {
                viewController.cellValue = event
                viewController.events = [event]
                self.navigationController?.pushViewController(viewController, animated: false)
            }
        }
    }
}

extension EventViewController: UITableViewDelegate, UITableViewDataSource, CreateEventVCDelegate, UpdateEventVCDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let text = eventList[indexPath.row].notes
        let height = text!.height(withConstrainedWidth: 289, font: UIFont.systemFont(ofSize: 15))
        return CGFloat(100 + height)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let text = eventList[indexPath.row].notes
        let height = text!.height(withConstrainedWidth: 289, font: UIFont.systemFont(ofSize: 15))
        return CGFloat(100 + height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
        cell.configureNotificationCell(event: eventList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            APIManagerEvent.deleteEvent(event: eventList[indexPath.row].id) { [self] response in
                if response == "success" {
                    didTapClose()
                }
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "Удалить") { (action, indexPath) in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        }
        
        deleteButton.backgroundColor = UIColor.white
        return [deleteButton]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "editEvent") {
            if let viewController = segue.destination as? EventListViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = eventList[index]
                viewController.events = [eventList[index]]
                viewController.eventDelegate = self
            }
        }
    }

    func didTapClose() {
        APIManagerEvent.getEvents { events in
            self.eventList = events
            self.tableView.reloadData()
            self.calendarView.reloadData()
        }
    }
    
}

extension UITableViewCell {
    var cellActionButtonLabel: UILabel? {
        for subview in self.superview?.subviews ?? [] {
            if String(describing: subview).range(of: "UISwipeActionPullView") != nil {
                for view in subview.subviews {
                    if String(describing: view).range(of: "UISwipeActionStandardButton") != nil {
                        for sub in view.subviews {
                            if let label = sub as? UILabel {
                                return label
                            }
                        }
                    }
                }
            }
        }
        return nil
    }

}
