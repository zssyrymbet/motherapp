//
//  CreateEventViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

protocol CreateEventVCDelegate: class {
    func didTapClose()
}

final class CreateEventViewController: UIViewController {

    @IBOutlet weak var viewOutlet: UIView!
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var eventDate: UITextField!
    @IBOutlet weak var eventTime: UITextField!
    @IBOutlet weak var notifyDate: UITextField!
    @IBOutlet weak var notifyTime: UITextField!
    @IBOutlet weak var notes: UITextField!
    weak var delegate: CreateEventVCDelegate?
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOutlet.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        viewOutlet.layer.shadowOpacity = 1
        viewOutlet.layer.shadowRadius = 3
        viewOutlet.layer.masksToBounds = false
        createDatePickerEvent()
        createDatePickerNotify()
        createTimePickerNotify()
        createTimePickerEvent()
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createDatePickerEvent() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedEvent))
        toolbar.setItems([doneButton], animated: true)

        eventDate.inputAccessoryView = toolbar
        eventDate.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
    }

    @objc func donePressedEvent() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        eventDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func createDatePickerNotify() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedNotify))
        toolbar.setItems([doneButton], animated: true)

        notifyDate.inputAccessoryView = toolbar
        notifyDate.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
    }

    @objc func donePressedNotify() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        notifyDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func createTimePickerNotify() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedNotifyTime))
        toolbar.setItems([doneButton], animated: true)

        notifyTime.inputAccessoryView = toolbar
        notifyTime.inputView = timePicker
        timePicker.datePickerMode = .time
        timePicker.preferredDatePickerStyle = .wheels
    }

    @objc func donePressedNotifyTime() {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.dateFormat = "hh:mm:ss"

        notifyTime.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
    }
    
    func createTimePickerEvent() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedEventTime))
        toolbar.setItems([doneButton], animated: true)

        eventTime.inputAccessoryView = toolbar
        eventTime.inputView = timePicker
        timePicker.datePickerMode = .time
        timePicker.preferredDatePickerStyle = .wheels
    }

    @objc func donePressedEventTime() {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.dateFormat = "hh:mm:ss"

        eventTime.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
    }

    @IBAction func createEvent(_ sender: Any) {
        APIManagerEvent.createEvent(date: eventDate.text!, name: eventName.text!, notes: notes.text!, remindAtDate: notifyDate.text!, remindAtTime: notifyTime.text!, time: eventTime.text!) { event in
            self.dismiss(animated: true, completion: nil)
            self.delegate?.didTapClose()
        }
    }
}
