//
//  CreateChildDataViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CreateChildDataViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var dateOfBirth: UITextField!
    @IBOutlet weak var diseases: UITextField!
    let datePicker = UIDatePicker()
    let genders = ["FEMALE", "MALE"]
    var pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        createDatePicker()
        gender.inputView = pickerView
    }
    
    func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: true)
        dateOfBirth.inputAccessoryView = toolbar
        dateOfBirth.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
    }
    
    @objc func donePressed() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        dateOfBirth.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        APIManagerChild.createChild(nickname: name.text!, gender: gender.text!, dateOfBirth: dateOfBirth.text!, disease: diseases.text!) { child in
            APIManagerChild.getChildListByParent { childList in
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    private func navigateToInterface() {
        APIManagerDict.getAllDicts {
            self.performSegue(withIdentifier: "NavigationBarController", sender: self)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        gender.text = genders[row]
        gender.resignFirstResponder()
    }
}
