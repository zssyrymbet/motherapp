//
//  RegistrationViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/5/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var fio: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var checkPassword: UITextField!
    
    @IBAction func haveAccountButton(_ sender: Any) {
        print("pressed")
        var windowz = UIApplication.shared.windows
        windowz.removeLast()
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        if (password.text == checkPassword.text) {
            APIManager.registration(email: email.text!, name: fio.text!,
                                    password: password.text!, phone: phoneNumber.text!) { UserData, error in
                if error != nil {
                    let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "questionnaireView") as! QuestionnaireViewController
                    viewController.userInfo = UserData
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        } else {
            print("Incorrect password!")
        }
    }
    
}



