//
//  ViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/3/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let manager = APIManager()
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var footerImage1: UIImageView!
    @IBOutlet weak var footerImage2: UIImageView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
       
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func fogotPasswordButton(_ sender: Any) {
        APIManager.resetPassword(email: email.text!) { response in
        }
    }
    
    @IBAction func LogInButton(_ sender: Any) {
        if (email.text?.isEmpty)! || (password.text?.isEmpty)! {
            return
        }
        
        APIManager.signIn(email: email.text!, password: password.text!) { UserData, error in
            if error != nil {
                let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                APIManagerDict.getAllDicts {
                    if UserData?.user != nil {
                        UserDefaults.standard.set(self.email.text!, forKey: "email")
                        UserDefaults.standard.set(self.password.text!, forKey: "password")
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "childView", sender: nil)
                    }
                }
            }
        }
    }

    @IBAction func createAccountButton(_ sender: Any) {
        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if(segue.identifier == "registrationView") {}
        }
    }
}


extension ViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
           case email:
               password.becomeFirstResponder()
           default:
               email.resignFirstResponder()
           }

           return true
    }

}

