//
//  QuestionnaireViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/6/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class QuestionnaireViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var questionnaireImage: UIImageView!
    @IBOutlet weak var gestationalAge: UITextField!
    @IBOutlet weak var position: UITextField!
    @IBOutlet weak var hospital: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var city: UITextField!
    var userInfo: UserData?
    
    var cities: [City] = []
    var hospitals: [Hospital] = []
    var user: User? = nil
    let pregnant = ["Да", "Нет"]
    let pickerView = UIPickerView()
    var cityId = 0
    var policlinicId = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))

        pickerView.delegate = self
        pickerView.dataSource = self
        city.delegate = self
        hospital.delegate = self
        position.delegate = self
        
        user = Singleton.shared.user
        city.inputView = pickerView
        hospital.inputView = pickerView
        position.inputView = pickerView
        

//        if Singleton.shared.user != nil {
            APIManagerDict.getCitiesName(completion: { cityList in
                print("cityList ", cityList)
                self.cities = cityList
            })
            
            APIManagerDict.getOrganizations { hospitalList in
                self.hospitals = hospitalList
            }
            
            APIManagerCategories.getCategories { categories in
//            }
        }
    }
    
    @IBAction func nextButton(_ sender: Any) {
        var pregnancy: Bool = false
        let pregnancyWeek = Int(gestationalAge.text!)
        
        if user != nil {
            if (position.text == "Да") {
                pregnancy = true
            }
            
            APIManager.updateUserData(user: user!, city: cityId, address: address.text!, polyclinic: policlinicId, isPregnant: pregnancy, pregnancyWeekCount: pregnancyWeek!, completion: { user, error in
                if error != nil {
                    let alert = UIAlertController(title: "Ошибка", message: error, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.performSegue(withIdentifier: "childView", sender: nil)
                }
            })
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       if city.isFirstResponder {
            return cities.count
       } else if hospital.isFirstResponder {
            return hospitals.count
       } else if position.isFirstResponder {
            return pregnant.count
       }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if city.isFirstResponder {
            return cities[row].name
        } else if hospital.isFirstResponder {
            return hospitals[row].name
        } else if position.isFirstResponder {
            return pregnant[row]
        }
        return nil

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var itemselected = ""
        if city.isFirstResponder {
            itemselected = cities[row].name
            city.text = itemselected
            cityId = cities[row].id
        } else if hospital.isFirstResponder {
            itemselected = hospitals[row].name
            hospital.text = itemselected
            policlinicId = hospitals[row].id
        } else if position.isFirstResponder {
            itemselected = pregnant[row]
            position.text = itemselected
        }
    }

    
}
