//
//  ChildrenListViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ChildrenListViewController: UIViewController {

    var childList: [Child] = []
    @IBOutlet weak var buttonCircle: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonCircle.layer.cornerRadius = buttonCircle.frame.size.width/2
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
        
        if Singleton.shared.userData != nil {
            childList = Singleton.shared.childList!
        }
    }
    
    @IBAction func skipButton(_ sender: Any) {
        navigateToInterface()
    }
    
    private func navigateToInterface() {
        APIManagerDict.getAllDicts {
            self.performSegue(withIdentifier: "NavigationBarController", sender: self)
        }
    }
    
    @IBAction func addButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension ChildrenListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildCell", for: indexPath) as! ChildTableViewCell
        cell.configureChildCell(child: childList[indexPath.row])
        return cell
    }
    
}
