//
//  SurveyGroupViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SurveyGroupViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var groupList: [SurveyGroup] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.setupNavBar()
        
        APIManagerSurvey.getSurveyGroups { groups in
            self.groupList = groups
            self.tableView.reloadData()
        }
    }
    
    private func setupNavBar() {
       self.navigationItem.title = "Опрос про развитие ребенка"
       self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
   }
}

extension SurveyGroupViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyGroupCell", for: indexPath) as! SurveyGroupTableViewCell
        cell.configureGroupCell(surveyGroup: groupList[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "startSurvey") {
            if let viewController = segue.destination as? ChildSurveyViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = groupList[index]
            }
        }
    }
    
}
