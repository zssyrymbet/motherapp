//
//  SurveyInstructionViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SurveyInstructionViewController: UIViewController {
    
    var cellValue: [SurveyQuestionList]?
    var name: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavBar()
    }
    
    private func setupNavBar() {
        self.navigationItem.title = "Опрос про развитие ребенка"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if cellValue != nil && segue.destination is SurveyQuestionnaireViewController {
            let vc = segue.destination as? SurveyQuestionnaireViewController
            vc?.data = cellValue
            vc?.nameOfQuestionnaire = name
        }
    }
}
