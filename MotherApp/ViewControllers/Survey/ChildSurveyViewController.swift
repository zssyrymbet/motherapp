//
//  ChildSurveyViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ChildSurveyViewController: UIViewController {

    var cellValue: SurveyGroup?
    @IBOutlet weak var age: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavBar()
    }
    
    private func setupNavBar() {
        self.navigationItem.title = "Опрос про развитие ребенка"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        APIManagerSurvey.startSurvey(month: (age.text! as NSString).integerValue, groupId: (cellValue?.id)!) { [self] survey in
            if survey.count != 0 && segue.destination is SurveyInstructionViewController {
                let vc = segue.destination as? SurveyInstructionViewController
                vc?.cellValue = survey
                vc?.name = cellValue?.name
            }
        }
    }
}
