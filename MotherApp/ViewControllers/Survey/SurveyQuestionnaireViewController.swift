//
//  QuesstionnaireViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SurveyQuestionnaireViewController: UIViewController {
    
    var surveyQuestions: [SurveyQuestionList]? {
        didSet {
            guard let questions = surveyQuestions else {
                return
            }
//            surveyAnsers = questions.map { questionList in
//                let id = questionList.questions.first?.id ?? 0
//                return SurveyAnswer(questionId: id, answer: "NO")
//            }
        }
    }
    
    var surveyAnswers: [SurveyAnswer] = []
    var data: [SurveyQuestionList]!
    var nameOfQuestionnaire: String?
    @IBOutlet weak var questionnaireTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        tableView.delegate = self
        tableView.dataSource = self
        
        if data != nil {
            questionnaireTitle.text = nameOfQuestionnaire
            surveyQuestions = data
        }
    }
    
    private func setupNavBar() {
        self.navigationItem.title = "Опрос про развитие ребенка"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        APIManagerSurvey.sendAnswer(id: surveyQuestions![0].id!, answers: surveyAnswers) { response in
            if response == "LOW" {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "surveyAnswerVc") as! SurveyResultViewContrroller
                viewController.surveyContent = "wertyuiop"
                viewController.surveyTitle = "poiuytrtyuiop"
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

extension SurveyQuestionnaireViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveyQuestions![0].questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionnaireCell", for: indexPath) as! QuestionnaireTableViewCell
        cell.configureQuestionCell(question: surveyQuestions![0].questions[indexPath.row])
        cell.onAnswer = { [weak self] id, answer in
            while let index = self!.surveyAnswers.firstIndex(where: { (surveyAnswer) -> Bool in
                surveyAnswer.questionId == id
            }) {
                self!.surveyAnswers.remove(at: index)
            }
            self!.surveyAnswers.append(SurveyAnswer(questionId: id, answer: answer ? "YES" : "NO"))
        }
        return cell
    }
}
