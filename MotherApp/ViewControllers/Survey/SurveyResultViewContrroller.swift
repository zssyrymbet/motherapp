//
//  SurveyResultViewContrroller.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SurveyResultViewContrroller: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
    var surveyTitle: String?
    var surveyContent: String?
    
    override func viewDidLoad() {
        self.setupNavBar()
        if surveyTitle != nil && surveyContent != nil {
            name.text = surveyTitle
            about.text = surveyContent
        }
    }
    
    @IBAction func OKButton(_ sender: Any) {
        let homeView = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchViewController
        self.navigationController?.pushViewController(homeView, animated: true)
    }
    
    private func setupNavBar() {
        self.navigationItem.title = "Опрос про развитие ребенка"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style: .plain, target: nil, action: nil)
    }
}
