//
//  ViolenceViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/10/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ViolenceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.layer.shadowOffset = CGSize(width: 0, height: 1)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowRadius = 2
        button.layer.masksToBounds = false
        
        riskOutlet.layer.shadowOffset = CGSize(width: 0, height: 1)
        riskOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        riskOutlet.layer.shadowOpacity = 1
        riskOutlet.layer.shadowRadius = 2
        riskOutlet.layer.masksToBounds = false
        
        if Singleton.shared.user != nil {
            userName.text = "Здравствуйте, \(Singleton.shared.user?.name ?? "Alina")!"
        }
    }
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var riskOutlet: UIButton!
    
    @IBAction func violenceButton(_ sender: Any) {
        APIManagerDict.getArticlesByCategoryId(id: 68) { [self] article in
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! ArticleDetailViewController
            viewController.cellValue = article[0]
            viewController.type = "smallCategories"
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    @IBAction func callButton(_ sender: Any) {
        let url: NSURL = URL(string: "TEL://1409")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func risksButton(_ sender: Any) {
        APIManagerDict.getArticlesByCategoryId(id: 69) { [self] article in
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! ArticleDetailViewController
            viewController.cellValue = article[0]
            viewController.type = "smallCategories"
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}

extension UIImageView {
    func downloadIcon(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloadIcon(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
