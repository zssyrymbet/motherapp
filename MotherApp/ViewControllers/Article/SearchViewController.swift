//
//  SearchViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var covidButtonOutlet: UIButton!
    @IBOutlet weak var questionnaireOutlet: UIButton!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var locationName: UILabel!
    
    var user : User? = nil
    var categories = [Category]()
    var filteredData: [Category]!
    var isSearching = false
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyle()
        user = Singleton.shared.user
        search.delegate = self
        search.enablesReturnKeyAutomatically = false
        search.barTintColor = UIColor.green
        filteredData = categories
        locationManager = CLLocationManager()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
        
        if user != nil {
            userName.text = "Здравствуйте, \(user?.name ?? "")!"
            APIManagerCategories.getCategories { categoryList in
                self.categories = categoryList
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func exitBtn(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "password")
        UserDefaults.standard.synchronize()
        let homeView = self.storyboard?.instantiateViewController(withIdentifier: "LogIn") as! ViewController
        self.navigationController?.pushViewController(homeView, animated: true)
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategotyCards", for: indexPath) as! CardsCollectionViewCell
        if isSearching {
            cell.category = filteredData[indexPath.row]
        } else {
            cell.category = categories[indexPath.row]
        }
        return cell
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CategoryView" {
            let viewController = segue.destination as! MainPageViewController
            if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                viewController.cellValue = categories[indexPath.row]
            }
        }
    }
    
    func setStyle() {
        covidButtonOutlet.layer.shadowOffset = CGSize(width: 0, height: 1)
        covidButtonOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        covidButtonOutlet.layer.shadowOpacity = 1
        covidButtonOutlet.layer.shadowRadius = 3
        covidButtonOutlet.layer.masksToBounds = false
        
        
        questionnaireOutlet.layer.shadowOffset = CGSize(width: 0, height: 1)
        questionnaireOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        questionnaireOutlet.layer.shadowOpacity = 1
        questionnaireOutlet.layer.shadowRadius = 3
        questionnaireOutlet.layer.masksToBounds = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            collectionView.reloadData()
        } else {
            isSearching = true
            filteredData = categories.filter({$0.name.lowercased().contains(searchBar.text!.lowercased() )})
            collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
}

