//
//  FavouriteListViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/20/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class FavouriteListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var articles = [Article]()
    var cellValue: Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        if Singleton.shared.userData != nil {
            APIManagerDict.getFavouriteArticles(completion: { articleList in
                self.articles = articleList
                self.tableView.reloadData()
            })
        }
    }
    
    
}

extension FavouriteListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleTableViewCell
        let articleCell = articles[indexPath.row]
            cell.article = articleCell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath : NSIndexPath = self.tableView.indexPathForSelectedRow! as NSIndexPath
        let viewController = segue.destination as! ArticleDetailViewController
        viewController.cellValue = articles[indexPath.row]
        viewController.type = articles[indexPath.row].topic
        self.navigationController?.pushViewController(viewController, animated: false)
    }
}
