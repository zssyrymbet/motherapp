//
//  ArticleDetailViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/18/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ArticleDetailViewController: UIViewController {

    @IBOutlet weak var titleOfArticle: UILabel!
    @IBOutlet weak var categoryType: UILabel!
    @IBOutlet weak var text: UITextView!
    @IBOutlet weak var articleDate: UILabel!
    @IBOutlet weak var saveToFav: UILabel!
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    
    var cellValue: Article?
    var favList: [Article]?
    var type: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if cellValue != nil {
            titleOfArticle.text = cellValue?.topic
            categoryType.text = type
            self.text.text = cellValue?.text.htmlString
            icon.download(from: cellValue!.iconUrl)
            
            if cellValue?.createdDate != nil {
                let inputFormatter = DateFormatter()
                inputFormatter.locale = Locale(identifier: "en_US_POSIX")
                inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let showDate = inputFormatter.date(from: (cellValue?.createdDate)!)
                inputFormatter.dateFormat = "dd.MM.yyyy"
                if showDate != nil {
                    articleDate.text = inputFormatter.string(from: showDate!)
                } else {
                    articleDate.text = "11.02.2021"
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        APIManagerDict.getFavouriteArticles(completion: { [self] articleList in
            for article in articleList {
                if article.id == cellValue?.id {
                    self.likeIcon.tintColor = UIColor.red
                    self.saveToFav.textColor = UIColor.red
                    self.saveToFav.text = "Удалить из избранного"
                }
            }
        })
    }
    
    @IBAction func saveToFavButton(_ sender: Any) {
        APIManagerDict.saveToFavourites(articleId: (cellValue?.id)!) { response in
            if response == "success" {
                self.likeIcon.tintColor = UIColor.red
                self.saveToFav.textColor = UIColor.red
                self.saveToFav.text = "Удалить из избранного"
            }
        }
    }
    
    @IBAction func deleteFromFav(_ sender: Any) {
        APIManagerDict.deleteFromFavourites(articleId: (cellValue?.id)!) { response in
            if response == "success" {
                self.likeIcon.tintColor = UIColor(red: 0.82, green: 0.63, blue: 0.68, alpha: 1.0)
                self.saveToFav.textColor = UIColor(red: 0.82, green: 0.63, blue: 0.68, alpha: 1.0)
                self.saveToFav.text = "Добавить в избранное"
            }
        }
    }
}

extension Data {
    var htmlAttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var htmlString: String { htmlAttributedString?.string ?? "" }
}

extension StringProtocol {
    var htmlAttributedString: NSAttributedString? {
        Data(utf8).htmlAttributedString
    }
    var htmlString: String {
        htmlAttributedString?.string ?? ""
    }
}

extension UIImageView {
    func download(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func download(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


