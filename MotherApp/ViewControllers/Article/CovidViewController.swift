//
//  CovidViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CovidViewController: UIViewController {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var categoryCards = [CategoryCard]()
    var filteredData = [CategoryCard]()
    var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        search.delegate = self
        search.enablesReturnKeyAutomatically = false
        filteredData = categoryCards
        
        APIManagerCategories.getCategoriesById(id: 20) { [self] cardsList in
            categoryCards = cardsList
            self.tableView.reloadData()
        }
    }
}

extension CovidViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return categoryCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "covidArticleCell", for: indexPath) as! CovidTableViewCell
        if isSearching {
            cell.configureCovidCell(covid: filteredData[indexPath.row])
        } else {
            cell.configureCovidCell(covid: categoryCards[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = categoryCards[indexPath.row].id
        APIManagerDict.getArticlesByCategoryId(id: id) { [self] article in
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! ArticleDetailViewController
            viewController.cellValue = article[0]
            viewController.type = "LARGE"
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            tableView.reloadData()
        } else {
            isSearching = true
            filteredData = categoryCards.filter({$0.topic.lowercased().contains(searchBar.text!.lowercased() )})
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
}
