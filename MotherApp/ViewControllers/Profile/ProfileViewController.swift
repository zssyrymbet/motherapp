//
//  ProfileViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonCircle: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var categories = [Category]()
    var childList: [Child] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
            
        if Singleton.shared.userData != nil {
            APIManagerChild.getChildListByParent { childs in
                self.childList = childs
                self.tableView.reloadData()
            }
            
            APIManagerCategories.getCategoriesProfile { categoryList in
                self.categories = categoryList
                self.collectionView.reloadData()
            }
        }
        
        buttonCircle.layer.cornerRadius = buttonCircle.frame.size.width/2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        APIManagerChild.getChildListByParent { children in
            self.childList = children
            self.tableView.reloadData()
        }
    }
    
    @IBAction func motherPage(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return childList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildCell", for: indexPath) as! ChildTableViewCell
        cell.configureChildCell(child: childList[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ChildProfileEditVC") {
            if let viewController = segue.destination as? ChildProfileEditViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = childList[index]
            }
        }
    }
}

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CardsCollectionViewCell
        let categoryCell = categories[indexPath.row]
        cell.category = categoryCell
        return cell
    }
}

