//
//  ProfileMotherViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/18/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ProfileMotherViewController: UIViewController {

    @IBOutlet weak var childInfo: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        viewData.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewData.layer.shadowColor = UIColor.lightGray.cgColor
        viewData.layer.shadowOpacity = 1
        viewData.layer.shadowRadius = 2
        viewData.layer.masksToBounds = false
        
        APIManagerChild.getProfileInfo { profile in
            self.childInfo.text = profile.text
            self.weight.text = String(profile.weight)
            self.height.text = String(profile.height)
        }
        
        if Singleton.shared.userData != nil {
            APIManagerCategories.getCategoriesProfile { categoryList in
                self.categories = categoryList
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func childPage(_ sender: Any) {
        
    }
    
}

extension ProfileMotherViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CardsCollectionViewCell
         let categoryCell = categories[indexPath.row]
        cell.category = categoryCell
        return cell
    }
}


