//
//  ChildProfileEditViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 29.03.2021.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ChildProfileEditViewController: UIViewController {

    @IBOutlet weak var diseases: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var dateOfBirth: UITextField!
    @IBOutlet weak var changeBtnOutlet: UIButton!
    let datePicker = UIDatePicker()
    var cellValue: Child?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createDatePicker()
        
        if cellValue != nil {
            print("cellValue ", cellValue)
            name.text = cellValue?.nickname
            diseases.text = cellValue?.disease
            gender.text = cellValue?.gender
            dateOfBirth.text = cellValue?.birthDate
            
            let inputFormatter = DateFormatter()
            inputFormatter.locale = Locale(identifier: "en_US_POSIX")
            inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let showDate = inputFormatter.date(from: (cellValue?.birthDate)!)
            if showDate != nil {
                inputFormatter.dateFormat = "dd.MM.yyyy"
                dateOfBirth.text = inputFormatter.string(from: showDate!)
            } else {
                dateOfBirth.text = ""
            }
        }
    }

    func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: true)
        dateOfBirth.inputAccessoryView = toolbar
        dateOfBirth.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
    }

    @objc func donePressed() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none

        dateOfBirth.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        changeBtnOutlet.setTitle("Сохранить", for: .normal)
        return true
    }


    @IBAction func changeButton(_ sender: Any) {
        APIManagerChild.updateChildInfo(childId: (cellValue?.id)!, nickname: name.text!, gender: gender.text!, dateOfBirth: dateOfBirth.text!, disease: diseases.text!) { response in
            if response == "success" {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        APIManagerChild.deleteChild(childId: (cellValue?.id)!) { response in
            if response == "success" {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
