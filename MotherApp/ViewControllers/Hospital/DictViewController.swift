//
//  DictViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/3/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class DictViewController: UIViewController {

    var hospitalList: [Hospital] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    var filteredData = [Hospital]()
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
        hospitalList = Singleton.shared.hospitalList!
        search.delegate = self
        search.enablesReturnKeyAutomatically = false
        filteredData = hospitalList
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension DictViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return hospitalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath) as! HospitalTableViewCell
        if isSearching {
            cell.configureHospitalCell(hospital: filteredData[indexPath.row])
        } else {
            cell.configureHospitalCell(hospital: hospitalList[indexPath.row])
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "HospitalDataViewController") {
            if let viewController = segue.destination as? HospitalDetailViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = hospitalList[index]
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            tableView.reloadData()
        } else {
            isSearching = true
            filteredData = hospitalList.filter({$0.name.lowercased().contains(searchBar.text!.lowercased() )})
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
    
}
