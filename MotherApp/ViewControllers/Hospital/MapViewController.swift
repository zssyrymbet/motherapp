//
//  MapViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    var hospitalList: [Hospital] = []
    var manager = CLLocationManager()
    var isInitiallyZoomedToUserLocation: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
        hospitalList = Singleton.shared.hospitalList!
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        createAnnotation(locations: hospitalList)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        APIManagerDict.getOrganizations { [self] hospitals in
            self.hospitalList = hospitals
            self.tableView.reloadData()
            createAnnotation(locations: hospitalList)
        }
    }
    
    func createAnnotation (locations: [Hospital]) {
        for location in hospitalList {
            let annotation = MKPointAnnotation()
            annotation.title = location.name
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.lon , longitude: location.lat)
            mapView.addAnnotation(annotation)
        }
    }
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hospitalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath) as! HospitalTableViewCell
        cell.configureHospitalCell(hospital: hospitalList[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "HospitalDataViewController") {
            if let viewController = segue.destination as? HospitalDetailViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = hospitalList[index]
            }
        }
    }
    
    func locationManager(_manager: CLLocationManager,
                         didUpdateLoccations locations: [CLLocation]) {
        let span = MKCoordinateSpan.init(latitudeDelta: 0.01, longitudeDelta: 0.01)
        _ = MKCoordinateRegion(center: locations[0].coordinate, span: span)
        print(locations[0].coordinate)
        mapView.showsUserLocation = true
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !isInitiallyZoomedToUserLocation {
           isInitiallyZoomedToUserLocation = true
           mapView.showAnnotations([userLocation], animated: true)
        }
    }
    
}
