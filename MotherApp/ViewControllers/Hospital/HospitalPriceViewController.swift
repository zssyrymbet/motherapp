//
//  HospitalPriceViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/3/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class HospitalPriceViewController: UIViewController {

    var cellValue: Service?
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var image: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if cellValue != nil {
            service.text = cellValue?.name
            price.text = String((cellValue?.price)!)
            location.text = "Aлматы, Казахстан"
        }
    }

    @IBAction func informationButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension UIImageView {
    func setIconHospital(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func setIconHospital(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

