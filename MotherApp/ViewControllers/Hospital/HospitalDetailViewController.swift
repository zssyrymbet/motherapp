//
//  HospitalDetailViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class HospitalDetailViewController: UIViewController {

    @IBOutlet weak var organizationName: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var telephoneNumber: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var image: UIImageView!
    var cellValue: Hospital?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if cellValue != nil {
            organizationName.text = cellValue?.name
            address.text = cellValue?.address
            type.text = cellValue?.type.name
            telephoneNumber.text = cellValue?.phone
            location.text = cellValue?.address
            if cellValue!.iconUrl != nil {
                image.setHospitalIcon(from: cellValue!.iconUrl)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ServiceView") {
            if let viewController = segue.destination as? HospitalPriceViewController {
                viewController.cellValue = cellValue?.serviceList[0]
            }
        }
    }
}

extension UIImageView {
    func setHospitalIcon(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func setHospitalIcon(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
