//
//  CardsCollectionViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/30/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    var category: Category? {
        didSet {
            name.text = category?.name
            if category!.iconUrl != nil {
                image.setIcon(from: category!.iconUrl)
            }
        }
    }
}

extension UIImageView {
    func setIcon(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func setIcon(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
