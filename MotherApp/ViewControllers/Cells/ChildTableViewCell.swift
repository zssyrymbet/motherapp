//
//  ChildTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ChildTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    
    func configureChildCell(child: Child!){
        name.text = child.nickname
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: child.birthDate!)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: date ?? Date())
        let calculatedAge = getAge(date: dateString)
        
        if (calculatedAge.0 != 0) {
            age.text = "\(calculatedAge.0) Год, \(calculatedAge.1) Месяц"
        } else if ((calculatedAge.0 != 0)){
            age.text = "\(calculatedAge.1) Месяц"
        }
    }
    
    func getAge(date: String) -> (Int,Int,Int) {
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "en_US_POSIX")
        dateFormater.dateFormat = "yyyy-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)
        let calender = Calendar.current
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
        dateOfBirth!, to: Date())
        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 7, right: 0))
    }
}
