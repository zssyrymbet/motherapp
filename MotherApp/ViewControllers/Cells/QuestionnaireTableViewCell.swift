//
//  QuestionnaireTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class QuestionnaireTableViewCell: UITableViewCell {

    @IBOutlet weak var switchYes: UISwitch!
    @IBOutlet weak var switchNo: UISwitch!
    @IBOutlet weak var name: UILabel!
    var onAnswer: ((Int, Bool) -> Void)?
    var id: Int?
         
    func configureQuestionCell(question: SurveyQuestion){
        self.name.text = question.text
        self.id = question.id
    }
    
    @IBAction func answerYes(_ sender: UISwitch) {
        onAnswer?(id!, true)
    }
    
    @IBAction func answerNo(_ sender: UISwitch) {
        onAnswer?(id!, false)
    }
}
