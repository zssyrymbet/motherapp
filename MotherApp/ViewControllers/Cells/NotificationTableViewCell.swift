//
//  NotificationTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notificationItemView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var date: UILabel!

    
    func configureNotificationCell(event: Event){
        self.name.text = event.name
        let inputFormatter = DateFormatter()
        inputFormatter.locale = Locale(identifier: "en_US_POSIX")
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let showDate = inputFormatter.date(from: (event.date!))
        inputFormatter.dateFormat = "dd.MM.yyyy"
        
        if showDate != nil {
            time.text = inputFormatter.string(from: showDate!)
        } else {
            time.text = ""
        }
        self.notes.text = event.notes
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm:ss a"
        let eventTime = timeFormatter.date(from: (event.time)!)
        timeFormatter.dateFormat = "hh:mm"
        self.date.text = timeFormatter.string(from: eventTime!)
        date.layer.cornerRadius = 5
        date.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()
            contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
            cellActionButtonLabel?.textColor = .red
    }
}
