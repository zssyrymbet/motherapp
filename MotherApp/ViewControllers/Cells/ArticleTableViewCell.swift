//
//  ArticleTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var icn: UIImageView!
    
    var article:  Article? {
        didSet {
            name.text = article?.topic
            let inputFormatter = DateFormatter()
            inputFormatter.locale = Locale(identifier: "en_US_POSIX")
            inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let showDate = inputFormatter.date(from: (article?.createdDate)!)
            inputFormatter.dateFormat = "dd.MM.yyyy"
            if showDate != nil {
                about.text = inputFormatter.string(from: showDate!)
            } else {
                about.text = "article content"
            }
            if article!.iconUrl != nil {
                icn.setArticleIcon(from: article!.iconUrl)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 7, right: 0))
    }
}

extension UIImageView {
    func setArticleIcon(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func setArticleIcon(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
