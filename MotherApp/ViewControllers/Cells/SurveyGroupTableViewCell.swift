//
//  SurveyGroupTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SurveyGroupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    
    func configureGroupCell(surveyGroup: SurveyGroup){
        self.name.text = surveyGroup.name
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 7, right: 0))
    }

}
