//
//  CreateChildDataViewController.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CreateChildViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var childImage: UIImageView!
    @IBAction func addChildButton(_ sender: Any) {
    }

    @IBAction func skipButton(_ sender: Any) {
        self.performSegue(withIdentifier: "NavigationBarController", sender: self)
    }
}
