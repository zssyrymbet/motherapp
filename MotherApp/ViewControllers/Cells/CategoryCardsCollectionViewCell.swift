//
//  CategoryCardsCollectionViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/10/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CategoryCardsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icon: UIImageView!
       
    var category:  CategoryCard? {
       didSet {
            name.text = category?.topic
            if category!.iconUrl != nil {
                icon.downloaded(from: category!.iconUrl)
            }
       }
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

