//
//  CardCategoryTableViewCell.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class CardCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    func configureCardCategoryCell(category: Category){
        self.title.text = category.name
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.shadowOffset = CGSize(width: 0, height: 1)
        contentView.layer.shadowColor = UIColor.lightGray.cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowRadius = 3
        contentView.layer.masksToBounds = false
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 12, right: 0))
    }
}
