//
//  ChildCardInfo.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct ChildCardInfo : Codable {
    var id: Int!
    var text: String!
    var childCardCategory: ChildCardCategory!
    var childCard: ChildCard!
}

struct ChildCardInfoResponses : Codable {
    var data: ChildCardInfo
}


