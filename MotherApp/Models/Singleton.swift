//
//  Singleton.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
final class Singleton: NSObject {
    
    static let shared: Singleton = {
        let instance = Singleton()
        return instance
    }()
    
    private override init() {}

    var user: User?
    var userData: UserData?
    var child: Child?
    var cityList: [City]?
    var categories: [Category]?
     var categoryCards: [CategoryCard]?
    var childList: [Child]?
    var hospitalList: [Hospital]?
    var articles: [Article]?
    var profile: Profile?
    var surveyQuestionList: [SurveyQuestionList]?
    
    func createUser(User: User) {
        user = User
    }
    
    func createUserDaata(UserData: UserData) {
        userData = UserData
    }
    
    func createChild(Child: Child) {
        child = Child
    }
    
    func saveCategories(Category: [Category]) {
        categories = Category
    }
    
    func saveCategoryCards(CategoryCard: [CategoryCard]) {
        categoryCards = CategoryCard
    }
    
    func saveArticles(Article: [Article]) {
        articles = Article
    }
    
    func getChildList(Child: [Child]) {
        childList = Child
    }
    
    func getHospitalList(Hospital: [Hospital]) {
        hospitalList = Hospital
    }
    
    func getCityList(City: [City]) {
        cityList = City
    }
    
    func createProfile(Profile: Profile) {
        profile = Profile
    }
    
    func saveQuestions(SurveyQuestionList: [SurveyQuestionList]) {
        surveyQuestionList = SurveyQuestionList
    }


}
