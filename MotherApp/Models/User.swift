//
//  User.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/6/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct User : Codable {
    var id: Int!
    var name: String!
    var email: String!
    var phone: String!
    var gender: String!
    var cityID: Int!
    var address: String!
    var polyclinicId: Int!
    var isPregnant: Bool!
    var pregnancyWeekCount: Int!
    
    init(id: Int, name: String, email: String, phone: String,
         gender: String, cityID: Int, address: String, polyclinicId: Int, isPregnant: Bool, pregnancyWeekCount: Int) {
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone
        self.gender = gender
        self.cityID = cityID
        self.address = address
        self.polyclinicId = polyclinicId
        self.isPregnant = isPregnant
        self.pregnancyWeekCount = pregnancyWeekCount
    }
}

struct UserData : Codable {
    var user : User!
    var token: String!
    
    init(user: User, token: String) {
        self.user = user
        self.token = token
    }
}

struct UserDataResponse: Codable {
    var data: UserData
}

