//
//  Profile.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/18/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct Profile : Codable {
    var text: String
    var isPregnant: Bool
    var weight: Double
    var height: Double
    
    init(text: String, isPregnant: Bool, weight: Double, height: Double) {
        self.text = text
        self.isPregnant = isPregnant
        self.weight = weight
        self.height = height
    }
}

struct ProfileResponses : Codable {
    var data: Profile
}
