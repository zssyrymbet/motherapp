//
//  ChildCardCategory.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct ChildCardCategory : Codable {
    var id: Int!
    var name: String

    init(id: Int, name: String) {
        self.name = name
        self.id = id
    }
}

struct ChildCardCategoryResponses : Codable {
    var data: ChildCardCategory
}
