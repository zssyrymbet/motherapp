//
//  Article.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Article: Codable {
    var id: Int!
    var topic: String!
    var text: String!
    var categoryList: [Category?]
    var iconUrl: String
    var createdDate: String
    
    init(id: Int, topic: String, text: String, categoryList: [Category], iconUrl: String, createdDate: String) {
        self.id = id
        self.text = text
        self.topic = topic
        self.categoryList = categoryList
        self.iconUrl = iconUrl
        self.createdDate = createdDate
    }
}

struct ArticleResponses : Codable {
    var data: Article
}




