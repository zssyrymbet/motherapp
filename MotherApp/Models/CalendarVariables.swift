//
//  CalendarVariables.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/31/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import Foundation

let date = Date()
let calendar = Calendar.current

let day = calendar.component(.day , from: date)
var weekday = calendar.component(.weekday, from: date) - 1
var month = calendar.component(.month, from: date) - 1
var year = calendar.component(.year, from: date)
