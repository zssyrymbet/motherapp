//
//  City.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct City : Codable {
    var name: String!
    var id: Int!
    var isActive: Int!

    init(name: String, id: Int, isActive: Int) {
        self.name = name
        self.id = id
        self.isActive = isActive
    }
}

struct CityResponse : Codable {
    var data: City
}

struct Cities {
    var cities: [City]
}
