//
//  Category.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct Category : Codable {
    var id: Int
    var code: String
    var type: String
    var name: String
    var iconUrl: String

    init(id: Int, code: String, type: String, name: String, iconUrl: String) {
        self.name = name
        self.id = id
        self.code = code
        self.type = type
        self.iconUrl = iconUrl
    }
}

struct CategoryResponse : Codable {
    var data: Category
}
