//
//  ChildCard.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct ChildCard : Codable {
    var id: Int!
    var name: String

    init(id: Int, name: String) {
        self.name = name
        self.id = id
    }
}

struct ChildCardResponses : Codable {
    var data: ChildCard
}
