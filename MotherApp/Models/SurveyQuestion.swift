//
//  SurveyQuestion.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct SurveyQuestion : Codable {
    var id: Int!
    var text: String!
    var riskLevel: String!
    
    init(id: Int, text: String, riskLevel: String) {
        self.id = id
        self.text = text
        self.riskLevel = riskLevel
    }
}

struct SurveyQuestionResponse : Codable {
    var data: SurveyQuestion
}

