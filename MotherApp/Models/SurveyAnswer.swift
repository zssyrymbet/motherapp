//
//  SurveyAnswer.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct SurveyAnswer : Codable {
    var questionId: Int
    var answer: String
    
    init(questionId: Int, answer: String) {
        self.questionId = questionId
        self.answer = answer
    }
}

struct SurveyAnswerResponse : Codable {
    var data: SurveyAnswer
}

extension Encodable {
    var convertToString: String? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
