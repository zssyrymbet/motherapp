//
//  CategoryCard.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/10/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct CategoryCard : Codable {
    var id: Int
    var topic: String
    var iconUrl: String

    init(id: Int, topic: String, iconUrl: String) {
        self.topic = topic
        self.id = id
        self.iconUrl = iconUrl
    }
}

struct CategoryCardResponse : Codable {
    var data: CategoryCard
}
