//
//  Child.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Parent: Codable {
    var id: Int
}

struct ParentResponse: Codable {
    var parent: Parent!
}



struct Child : Codable {
    var id: Int!
    var nickname: String!
    var birthDate: String!
    var gender: String!
    var disease: String!
    var parentId: Int!
    var height: Int!
    var weight: Int!
    
    init(id: Int, nickname: String, birthDate: String, gender: String, disease: String, parentId: Int!, height: Int, weight: Int) {
           self.id = id
           self.nickname = nickname
           self.birthDate = birthDate
           self.gender = gender
           self.disease = disease
           self.parentId = parentId
           self.height = height
           self.weight = weight
       }
}

struct ChildResponse: Codable {
    var data: Child!
}
