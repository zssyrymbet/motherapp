//
//  SurveyQuestionList.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct SurveyQuestionList : Codable {
    var id: Int?
    var questions: [SurveyQuestion]!
    var riskLevel: String!
    
    init(id: Int, questions: [SurveyQuestion], riskLevel: String) {
        self.id = id
        self.questions = questions
        self.riskLevel = riskLevel
    }
}

struct SurveyQuestionListResponse : Codable {
    var data: SurveyQuestionList
}

