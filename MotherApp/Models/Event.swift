//
//  Event.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/20/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Event : Codable {
    var id: Int!
    var date: String!
    var name: String!
    var notes: String!
    var remindAtDate: String!
    var remindAtTime: String!
    var time: String!
    var userId: Int!
    
    init(id: Int, date: String, name: String, notes: String, remindAtDate: String, remindAtTime: String, time: String, userId: Int) {
        self.id = id
        self.date = date
        self.name = name
        self.notes = notes
        self.remindAtDate = remindAtDate
        self.remindAtTime = remindAtTime
        self.time = time
        self.userId = userId
    }
    
}

struct EventResponse : Codable {
    var data: Event
}
