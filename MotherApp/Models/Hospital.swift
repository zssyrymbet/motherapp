//
//  Hospital.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 10/24/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Hospital : Codable {
    var id: Int!
    var name: String!
    var info: String!
    var address: String!
    var phone: String!
    var type: Type!
    var city: City!
    var serviceList: [Service?]
    var lon: Double!
    var lat: Double!
    var iconUrl: String
    
    init(id: Int, name: String, info: String, address: String, phone: String, type: Type, city: City, serviceList: [Service], lon: Double, lat: Double, iconUrl: String) {
        self.id = id
        self.name = name
        self.info = info
        self.address = address
        self.phone = phone
        self.type = type
        self.city = city
        self.serviceList = serviceList
        self.lat = lat
        self.lon = lon
        self.iconUrl = iconUrl
    }
}

struct HospitalResponse : Codable {
    var data: Hospital
}


