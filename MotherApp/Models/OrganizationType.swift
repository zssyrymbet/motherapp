//
//  OrganizationType.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct OrganizationType : Codable {
    var id: Int!
    var code: String!
    var name: String

    init(id: Int, code: String, name: String) {
        self.name = name
        self.id = id
        self.code = code
    }
}

struct OrganizationTypeResponses : Codable {
    var data: OrganizationType
}
