//
//  Service.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//
import UIKit

struct Service : Codable {
    var id: Int!
    var price: Double!
    var name: String!
    
    init(id: Int, name: String, price: Double) {
        self.id = id
        self.name = name
        self.price = price
    }
}

struct ServiceResponse : Codable {
    var data: Service
}
