//
//  Type.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Type : Codable {
    var id: Int!
    var code: String!
    var name: String!
    
    init(id: Int, name: String, code: String) {
        self.id = id
        self.name = name
        self.code = code
    }
}

struct TypeResponse : Codable {
    var data: Type
}
