//
//  SurveyGroup.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
struct SurveyGroup : Codable {
    var id: Int!
    var name: String!
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}

struct SurveyGroupResponse : Codable {
    var data: SurveyGroup
}
