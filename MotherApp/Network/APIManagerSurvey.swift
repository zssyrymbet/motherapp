//
//  APISurvey.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 1/24/21.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class APIManagerSurvey {
    
    static func getSurveyGroups(completion: @escaping ([SurveyGroup]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/groups"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var groupList = [SurveyGroup]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                       for obj in array {
                           if let dict = obj as? NSDictionary {
                               let id = dict.value(forKey: "id")
                               let name = dict.value(forKey: "name")
                               let group = SurveyGroup(id: id as? Int ?? 0, name: name as! String)
                               groupList.append(group)
                           }
                        }
                    }
                    completion(groupList)
                break
                
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func startSurvey(month: Int, groupId: Int, completion: @escaping ([SurveyQuestionList]) -> ()) {
            let url: String = "http://78.140.223.188:7070/childcare/survey/start/\(month)/\(groupId)"
            var request = URLRequest(url:  NSURL(string: url)! as URL)
            request.httpMethod = "GET"
            request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            AF.request(request).responseJSON { response in
                var questionList = [SurveyQuestionList]()
                var questionnaires = [SurveyQuestion]()
                
                switch response.result {
                   case .success(let data):
                        let JSON = data as! NSDictionary
                        let data = JSON["data"]
                            if let dict = data as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let riskLevel = dict.value(forKey: "riskLevel")
                                let questions = dict.value(forKey: "questions") as! NSArray
                                for question in questions {
                                    if let item = question as? NSDictionary {
                                        let id = item.value(forKey: "id")
                                        let text = item.value(forKey: "text")
                                        let riskLevel = item.value(forKey: "riskLevel")
                                        let questionnaire = SurveyQuestion(id: id as? Int ?? 0, text: text as? String ?? "", riskLevel: riskLevel as? String ?? "")
                                        questionnaires.append(questionnaire)
                                    }
                                }
                                let surveyQuestions = SurveyQuestionList(id: id as? Int ?? 0, questions: questionnaires, riskLevel: riskLevel as? String ?? "")
                                questionList.append(surveyQuestions)
                        }
                        completion(questionList)
                    break
                    
                    case .failure(let error):
                        print(error)
                    break
                }
            }.resume()
    }
    
    static func sendAnswer(id: Int, answers: [SurveyAnswer], completion: @escaping (String) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/survey/\(id)"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let data = answers.map({ (element) -> [String: Any] in
            [
                "questionId": element.questionId,
                "answer": element.answer
            ]
        })

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data)
        } catch let error {
               print(error.localizedDescription)
        }

        AF.request(request).responseJSON { response in
            print("response ", response)
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["data"]
                    completion(data as! String)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
}

