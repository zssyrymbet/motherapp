//
//  APIEvent.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 07.04.2021.
//  Copyright © 2021 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class APIManagerEvent {
    
    static func createEvent(date: String, name: String, notes: String, remindAtDate: String, remindAtTime: String, time: String, completion: @escaping (Event) -> ()) {
            
            var event: Event? = nil
            let userId = Singleton.shared.user?.id!

            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "d MMM yyyy"
            let dateEvent = dateFormatter.date(from: date)!
            let dateNotify = dateFormatter.date(from: remindAtDate)!
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let eventDate = dateFormatter.string(from: dateEvent)
            let notifyDate = dateFormatter.string(from: dateNotify)
      
            let data = [
                    "date": eventDate,
                    "name": name,
                    "remindAtDate": notifyDate,
                    "remindAtTime": remindAtTime,
                    "time": time,
                    "notes": notes,
                    "userId": userId as Any
                ] as [String : Any]
            
            let url: String = "http://78.140.223.188:7070/childcare/events"
            var request = URLRequest(url:  NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            } catch let error {
                   print(error.localizedDescription)
            }
            
            AF.request(request).responseJSON { response in
                print("response ", response)
                switch response.result {
                    case .success(let result):
                        let JSON = result as! NSDictionary
                        let data = JSON["data"] as? [String: Any]
                        if let data = data {
                            event = Event(id: data["id"] as? Int ?? 0, date: data["date"] as! String, name: data["name"] as! String, notes: data["notes"] as! String, remindAtDate: data["remindAtDate"] as! String, remindAtTime: " ", time: " ", userId: data["userId"] as? Int ?? 0)
                            completion(event!)
                        }
                    break
                    case .failure(let error):
                        print(error)
                    break
                }
            }
    }
    
    static func getEvents(completion: @escaping ([Event]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/events/my/old"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                       
        AF.request(request).responseJSON { response in
            var eventList = [Event]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let date = dict.value(forKey: "date")
                                let name = dict.value(forKey: "name")
                                let notes = dict.value(forKey: "notes")
                                let remindAtDate = dict.value(forKey: "remindAtDate")
                                let remindAtTime = dict.value(forKey: "remindAtTime")
                                let time = dict.value(forKey: "time")
                                let userId = dict.value(forKey: "userId")
                                let eventInfo = Event(id: id as? Int ?? 0, date: date as? String ?? "", name: name as? String ?? "", notes: notes as? String ?? "", remindAtDate: remindAtDate as? String ?? "", remindAtTime: remindAtTime as? String ?? "", time: time as? String ?? "", userId: userId as? Int ?? 0)
                                eventList.append(eventInfo)
                            }
                        }
                    }
                    completion(eventList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func updateEvent(id: Int, date: String, name: String, notes: String, remindAtDate: String, remindAtTime: String, time: String, completion: @escaping (String) -> ()) {
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateEvent = dateFormatter.date(from: date)!
            let dateNotify = dateFormatter.date(from: remindAtDate)!
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let eventDate = dateFormatter.string(from: dateEvent)
            let notifyDate = dateFormatter.string(from: dateNotify)

            let data = [
                    "id": id,
                    "date": eventDate,
                    "name": name,
                    "remindAtDate": notifyDate,
                    "remindAtTime": remindAtTime,
                    "time": time,
                    "notes": notes
                ] as [String : Any]
                
            let url: String = "http://78.140.223.188:7070/childcare/events"
            var request = URLRequest(url:  NSURL(string: url)! as URL)
            request.httpMethod = "PUT"
            request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            } catch let error {
                   print(error.localizedDescription)
            }
            
        AF.request(request).responseJSON { response in
            switch response.result {
            case .success(let result):
                let JSON = result as! NSDictionary
                let data = JSON["status"]
                completion(data as! String)
            break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    static func deleteEvent(event: Int, completion: @escaping (String) -> ()) {
        let data = ["ids": [event]]
        let url: String = "http://78.140.223.188:7070/childcare/events"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "DELETE"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        } catch let error {
               print(error.localizedDescription)
        }
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["status"]
                    completion(data as! String)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
}
