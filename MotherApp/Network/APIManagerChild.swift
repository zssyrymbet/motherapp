//
//  APIManagerChild.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class APIManagerChild {
    
    static func createChild(nickname: String, gender: String, dateOfBirth: String, disease: String, completion: @escaping (Child) -> ()) {
        
        var child: Child? = nil
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "d MMM yyyy"
        let date = dateFormatter.date(from: dateOfBirth)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let birthDate = dateFormatter.string(from: date)
                
        let data = [
                "nickname": nickname,
                "gender": gender,
                "birthDate": birthDate,
                "disease": disease,
                "height": 12,
                "weight": 5.0
            ] as [String : Any]
        let url: String = "http://78.140.223.188:7070/childcare/children"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        } catch let error {
               print(error.localizedDescription)
        }
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["data"] as? [String: Any]
                    if let data = data {
                        child = Child(id: data["id"] as? Int ?? 0, nickname: data["nickname"] as! String, birthDate: data["birthDate"] as! String, gender: data["gender"] as! String, disease: data["disease"] as! String, parentId: (data["parentId"] as? Int ?? 0), height: data["height"] as? Int ?? 0, weight: data["weight"] as? Int ?? 0)
                        Singleton.shared.createChild(Child: child!)
                        completion(child!)
                    }
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    
    static func getChildListByParent(completion: @escaping ([Child]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/children/my"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var childList = [Child]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let nickname = dict.value(forKey: "nickname")
                                let birthDate = dict.value(forKey: "birthDate")
                                let gender = dict.value(forKey: "gender")
                                let parentId = dict.value(forKey: "parentId")
                                let height = dict.value(forKey: "height")
                                let weight = dict.value(forKey: "weight")
                                let disease = dict.value(forKey: "disease")
                                let child = Child(id: id as? Int ?? 0, nickname: nickname as! String, birthDate: birthDate as! String, gender: gender as! String, disease: disease as! String, parentId: (parentId as? Int ?? 0), height: height as? Int ?? 0, weight: weight as? Int ?? 0)
                                childList.append(child)
                                Singleton.shared.getChildList(Child: childList)
                            }
                        }
                    }
                    completion(childList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getProfileInfo(completion: @escaping (Profile) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/profile"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                        if let dict = data as? NSDictionary {
                            let text = dict.value(forKey: "text")
                            let isPregnant = dict.value(forKey: "isPregnant")
                            let weight = dict.value(forKey: "weight")
                            let height = dict.value(forKey: "height")
                            let profile = Profile(text: text as? String ?? "", isPregnant: isPregnant as! Bool, weight: weight as? Double ?? 0.0, height: height as? Double ?? 0.0)
                            Singleton.shared.createProfile(Profile: profile)
                            completion(profile)
                        }
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getChildCardCategories(completion: @escaping ([ChildCardCategory]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/childCardCategory"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        AF.request(request).responseJSON { response in
            var childCategoryList = [ChildCardCategory]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let childCategory = ChildCardCategory(id: id as? Int ?? 0, name: name as! String)
                                childCategoryList.append(childCategory)
                            }
                        }
                    }
                    completion(childCategoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getChildCards(completion: @escaping ([ChildCard]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/childCards"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                       
        AF.request(request).responseJSON { response in
            var childCardList = [ChildCard]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let childCard = ChildCard(id: id as? Int ?? 0, name: name as! String)
                                childCardList.append(childCard)
                            }
                        }
                    }
                    completion(childCardList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getChildCardInfo(completion: @escaping ([ChildCardInfo]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/childCardCatInfo"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                       
        AF.request(request).responseJSON { response in
            var childCardList = [ChildCardInfo]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let text = dict.value(forKey: "text")
                                let childCardCategory = dict.value(forKey: "childCardCategory")
                                let childCard = dict.value(forKey: "childCard")
                                let childCardInfo = ChildCardInfo(id: id as? Int, text: text as? String, childCardCategory: childCardCategory as? ChildCardCategory, childCard: childCard as? ChildCard)
                                childCardList.append(childCardInfo)
                            }
                        }
                    }
                    completion(childCardList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    
    static func deleteChild(childId: Int, completion: @escaping (String) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/children/\(childId)"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "DELETE"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                       
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["status"]
                    completion(data as! String)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    static func updateChildInfo(childId: Int, nickname: String, gender: String, dateOfBirth: String, disease: String,
                            completion: @escaping (String) -> ()) {

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let date = dateFormatter.date(from: dateOfBirth)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let birthDate = dateFormatter.string(from: date)
        
        let data = [
                "nickname": nickname,
                "gender": gender,
                "birthDate": birthDate,
                "disease": disease,
                "id": childId as Any
            ] as [String : Any]
        
        
        let url: String = "http://78.140.223.188:7070/childcare/children"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        } catch let error {
               print(error.localizedDescription)
        }
        
        AF.request(request).responseJSON { response in
            switch response.result {
            case .success(let result):
                let JSON = result as! NSDictionary
                let data = JSON["status"]
                completion(data as! String)
            break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
}


