//
//  MotherApp Manager.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/6/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class APIManager {
    
    static func registration(email: String, name: String, password: String, phone: String,
                             completion: @escaping (UserData?, String?) -> ())
       {
        var user: User? = nil
        let data = [
                "email": email,
                "name": name,
                "password": password,
                "phone": phone
            ]
        
        AF.request("http://78.140.223.188:7070/childcare/auth/signup", method: HTTPMethod.post, parameters: data, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                    case .success(let result):
                        let JSON = result as! NSDictionary
                        
                        if Array(JSON)[0].value as! String != "success" {
                            completion(nil, "\(Array(JSON)[0].value)")
                        }
                        
                        let data = JSON["data"] as? [String: Any]
                        let token = data?["token"] as? String
                        let userData = data?["user"] as? [String: Any]
                        if let userData = userData {
                            user = User(id: userData["id"] as? Int ?? 0, name: userData["name"] as! String, email: userData["email"] as! String, phone: userData["phone"] as! String, gender: "", cityID: 0, address: "", polyclinicId: 0, isPregnant: false, pregnancyWeekCount: 0)
                            let userInfo = UserData(user: user!, token: token!)
                            Singleton.shared.createUserDaata(UserData: userInfo)
                            Singleton.shared.createUser(User: user!)
                            let token = Messaging.messaging().fcmToken
                            completion(userInfo, nil)
                        }
                    break
                    case .failure(let error):
                        print(error)
                    break
                }
        }
    }
    
    static func signIn(email: String, password: String, completion: @escaping (UserData?, String?) -> ()) {
        var user: User? = nil
        
        let data = [
            "username": email,
            "password": password,
        ]
        
        AF.request("http://78.140.223.188:7070/childcare/auth/login", method: .post, parameters: data, encoding: JSONEncoding.default)
        .responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    if Array(JSON)[0].value as! String != "success" {
                        completion(nil, "\(Array(JSON)[0].value)")
                    }
                    
                    let data = JSON["data"] as? [String: Any]
                    if let data = data {
                        let userData = data["user"] as? [String: Any]
                        let token = data["token"] as? String
                        user = User(id: userData!["id"] as? Int ?? 0, name: userData!["name"] as! String, email: userData!["email"] as! String, phone: userData!["phone"] as! String, gender: userData!["gender"] as! String, cityID: userData!["cityId"] as? Int ?? 0, address: userData!["address"] as! String, polyclinicId: userData!["polyclinicId"] as? Int ?? 0, isPregnant: false, pregnancyWeekCount: userData!["pregnancyWeekCount"] as? Int ?? 0)
                            Singleton.shared.createUser(User: user!)
                                if let user = Singleton.shared.user {
                                    let userInfo = UserData(user: user, token: token!)
                                    Singleton.shared.createUserDaata(UserData: userInfo)
                                    completion(userInfo, nil)
                                    self.registerToken()
                                }
                    }
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    static func updateUserData(user: User, city: Int, address: String, polyclinic: Int, isPregnant: Bool, pregnancyWeekCount: Int, completion: @escaping (User?, String?) -> ()) {
        
        var userData: User? = nil
        let data = [
            "email": user.email!,
            "name": user.name!,
            "phone": user.phone!,
            "id": user.id!,
            "cityId": city,
            "address": address,
            "polyclinicId": polyclinic,
            "isPregnant": isPregnant,
            "pregnancyWeekCount": pregnancyWeekCount,
            "gender": "FEMALE",
        ] as [String : Any]

        let url: String = "http://78.140.223.188:7070/childcare/users/dto"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        } catch let error {
               print(error.localizedDescription)
        }
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    
                    if Array(JSON)[0].value as! String != "success" {
                        completion(nil, "\(Array(JSON)[0].value)")
                    }
                    
                    let data = JSON["data"] as? [String: Any]
                    if let data = data {
                        userData = User(id: data["id"] as? Int ?? 0, name: data["name"] as! String, email: data["email"] as! String, phone: data["phone"] as! String, gender: data["gender"] as! String, cityID: data["cityId"] as? Int ?? 0, address: data["address"] as! String, polyclinicId: data["polyclinicId"] as? Int ?? 0, isPregnant: data["isPregnant"] as! Bool, pregnancyWeekCount: data["pregnancyWeekCount"] as? Int ?? 0)
                        Singleton.shared.createUser(User: userData!)
                        completion(userData!, nil)
                        self.registerToken()
                    }
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    static func registerToken() {
        let url: String = "http://78.140.223.188:7070/childcare/users/register/\(Messaging.messaging().fcmToken ?? "")"
            var request = URLRequest(url:  NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            AF.request(request).responseJSON { response in
            }.resume()
    }
    
    static func resetPassword(email: String, completion: @escaping (String) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/auth/sendReset/\(email)"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            print(response)
        }.resume()
    }
    
}

 




