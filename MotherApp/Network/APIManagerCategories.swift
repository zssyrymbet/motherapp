//
//  APICategories.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 12/19/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class APIManagerCategories {
    
    static func getCategories(completion: @escaping ([Category]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/categories/type/SMALL"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var categoryList = [Category]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let code = dict.value(forKey: "code")
                                let type = dict.value(forKey: "type")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let category = Category(id: id as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                categoryList.append(category)
                                Singleton.shared.saveCategories(Category: categoryList)
                            }
                        }
                    }
                    completion(categoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getCategoriesById(id: Int, completion: @escaping ([CategoryCard]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/articles/catId/\(id)"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var categoryList = [CategoryCard]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let topic = dict.value(forKey: "topic")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let category = CategoryCard(id: id as? Int ?? 0, topic: topic as! String, iconUrl: iconUrl as? String ?? "")
                                categoryList.append(category)
                                Singleton.shared.saveCategoryCards(CategoryCard: categoryList)
                            }
                        }
                    }
                    completion(categoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getCategoriesCard(completion: @escaping ([Category]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/categories/type/CARD"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var categoryList = [Category]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let code = dict.value(forKey: "code")
                                let type = dict.value(forKey: "type")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let category = Category(id: id as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                categoryList.append(category)
                                Singleton.shared.saveCategories(Category: categoryList)
                            }
                        }
                    }
                    completion(categoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getCategoriesProfile(completion: @escaping ([Category]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/categories/type/PROFILE"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var categoryList = [Category]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let code = dict.value(forKey: "code")
                                let type = dict.value(forKey: "type")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let category = Category(id: id as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                categoryList.append(category)
                                Singleton.shared.saveCategories(Category: categoryList)
                            }
                        }
                    }
                    completion(categoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getCategoriesChildProfile(completion: @escaping ([Category]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/categories/type/CHILD_PROFILE"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            var categoryList = [Category]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let code = dict.value(forKey: "code")
                                let type = dict.value(forKey: "type")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let category = Category(id: id as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                categoryList.append(category)
                                Singleton.shared.saveCategories(Category: categoryList)
                            }
                        }
                    }
                    completion(categoryList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func search(searchString: String, categoryId: Int, completion: @escaping ([Article]) -> ()) {
        let data = [
            "searchString": searchString,
            "categoryId": categoryId
        ] as [String : Any]

        let url: String = "http://78.140.223.188:7070/childcare/articles/search"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        } catch let error {
               print(error.localizedDescription)
        }
        
        AF.request(request).responseJSON { response in
            var articleList = [Article]()
            var categoryList = [Category]()
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["data"]
                    if let dict = data as? NSDictionary {
                        let id = dict.value(forKey: "id")
                        let topic = dict.value(forKey: "topic")
                        let text = dict.value(forKey: "text")
                        let iconUrl = dict.value(forKey: "iconUrl")
                        let createdDate = dict.value(forKey: "createdDate")
                        let catrgories = dict.value(forKey: "categoryList") as! NSArray
                        for category in catrgories {
                            if let item = category as? NSDictionary {
                                let categotyId = item.value(forKey: "id")
                                let code = item.value(forKey: "code")
                                let type = item.value(forKey: "type")
                                let name = item.value(forKey: "name")
                                let iconUrl = item.value(forKey: "iconUrl")
                                let articleCategory = Category(id: categotyId as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                categoryList.append(articleCategory)
                            }
                        }
                        let article = Article(id: id as? Int ?? 0, topic: topic as! String, text: text as! String, categoryList: categoryList, iconUrl: iconUrl as? String ?? "", createdDate: createdDate as? String ?? "")
                        articleList.append(article)
                        Singleton.shared.saveArticles(Article: articleList)
                        completion(articleList)
                }
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
}
}
