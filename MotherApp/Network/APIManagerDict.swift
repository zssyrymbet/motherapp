//
//  APIManagerDict.swift
//  MotherApp
//
//  Created by Zarina Syrymbet on 11/26/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class APIManagerDict {
    
    static func getCitiesName(completion: @escaping ([City]) -> ()) {
        
        let url: String = "http://78.140.223.188:7070/childcare/cities"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            print("responseCity ", response)
            var cityList = [City]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let isActive = dict.value(forKey: "isActive")
                                let city = City(name: name as! String, id: id as? Int ?? 0, isActive: isActive as? Int ?? 0)
                                cityList.append(city)
                                Singleton.shared.getCityList(City: cityList)
                            }
                        }
                    }
                    completion(cityList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getOrganizations(completion: @escaping ([Hospital]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/organizations"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        AF.request(request).responseJSON { response in
            var hospitalList = [Hospital]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                        let data = JSON["data"]
                        var hospitalType: Type?
                        var hospitalCity: City?
                        var hospitalService = [Service]()
                        if let array = data as? NSArray {
                            for obj in array {
                                if let dict = obj as? NSDictionary {
                                    let id = dict.value(forKey: "id")
                                    let name = dict.value(forKey: "name")
                                    let info = dict.value(forKey: "info")
                                    let address = dict.value(forKey: "address")
                                    let phone = dict.value(forKey: "phone")
                                    let lon = dict.value(forKey: "lon")
                                    let lat = dict.value(forKey: "lat")
                                    let iconUrl = dict.value(forKey: "iconUrl")
                                    
                                    if let typeData = dict.value(forKey: "type") as? NSDictionary {
                                        let typeId = typeData.value(forKey: "id")
                                        let typeCode = typeData.value(forKey: "code")
                                        let typeName = typeData.value(forKey: "name")
                                        hospitalType = Type(id: typeId as? Int ?? 0, name: typeName as! String, code: typeCode as! String)
                                    }
                                    
                                    if let cityData = dict.value(forKey: "city") as? NSDictionary {
                                        let cityId = cityData.value(forKey: "id")
                                        let cityName = cityData.value(forKey: "name")
                                        let cityActive = cityData.value(forKey: "isActive")
                                        hospitalCity = City(name: cityName as! String, id: cityId as? Int ?? 0, isActive: cityActive as? Int ?? 0)
                                    }
                                    
                                    if let services = dict.value(forKey: "serviceList") as? NSArray {
                                        for serviceData in services {
                                            if let service = serviceData as? NSDictionary {
                                                let serviceId = service.value(forKey: "id")
                                                let serviceName = service.value(forKey: "name")
                                                let price = service.value(forKey: "price")
                                                let service = Service(id: serviceId as? Int ?? 0, name: serviceName as! String, price: price as? Double ?? 0.0)
                                                hospitalService.append(service)
                                            }
                                        }
                                    }
                    
                                    let hospital = Hospital(id: id as? Int ?? 0, name: name as! String, info: info as! String, address: address as! String, phone: phone as! String, type: hospitalType!, city: hospitalCity!, serviceList: hospitalService, lon: lon as! Double, lat: lat as! Double, iconUrl: iconUrl as? String ?? "")
                                    hospitalList.append(hospital)
                                    Singleton.shared.getHospitalList(Hospital: hospitalList)
                                }
                            }
                        }
                        completion(hospitalList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getOrganizationTypes(completion: @escaping ([OrganizationType]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/organizationTypes"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        AF.request(request).responseJSON { response in
            var organizationTypes = [OrganizationType]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let name = dict.value(forKey: "name")
                                let code = dict.value(forKey: "code")
                                let organizationType = OrganizationType(id: id as? Int ?? 0, code: code as! String, name: name as! String)
                                organizationTypes.append(organizationType)
                            }
                        }
                    }
                    completion(organizationTypes)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }
    
    static func getArticlesByCategoryId(id: Int, completion: @escaping ([Article]) -> ()) {
            let url: String = "http://78.140.223.188:7070/childcare/articles/\(id)"
            var request = URLRequest(url:  NSURL(string: url)! as URL)
            request.httpMethod = "GET"
            request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            AF.request(request).responseJSON { response in
                var articleList = [Article]()
                var categoryList = [Category]()
                switch response.result {
                    case .success(let data):
                        let JSON = data as! NSDictionary
                        let data = JSON["data"]
                            if let dict = data as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let topic = dict.value(forKey: "topic")
                                let text = dict.value(forKey: "text")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let createdDate = dict.value(forKey: "createdDate")
                                let catrgories = dict.value(forKey: "categoryList") as! NSArray
                                for category in catrgories {
                                    if let item = category as? NSDictionary {
                                        let categotyId = item.value(forKey: "id")
                                        let code = item.value(forKey: "code")
                                        let type = item.value(forKey: "type")
                                        let name = item.value(forKey: "name")
                                        let iconUrl = item.value(forKey: "iconUrl")
                                        let articleCategory = Category(id: categotyId as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                        categoryList.append(articleCategory)
                                    }
                                }
                                let article = Article(id: id as? Int ?? 0, topic: topic as! String, text: text as! String, categoryList: categoryList, iconUrl: iconUrl as? String ?? "", createdDate: createdDate as? String ?? "")
                                articleList.append(article)
                                Singleton.shared.saveArticles(Article: articleList)
                                completion(articleList)
                        }
                    break
                    case .failure(let error):
                        print(error)
                    break
                }
            }.resume()
    }
    
    static func saveToFavourites(articleId: Int, completion: @escaping (String) -> ()) {
        let article = String(articleId)
        let url: String = "http://78.140.223.188:7070/childcare/users/fav"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = article.data(using: .utf8)!
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["status"]
                    completion(data as! String)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    static func deleteFromFavourites(articleId: Int, completion: @escaping (String) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/users/fav/\(articleId)"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "DELETE"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).responseJSON { response in
            switch response.result {
                case .success(let result):
                    let JSON = result as! NSDictionary
                    let data = JSON["status"]
                    completion(data as! String)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }
    }
    
    
    static func getFavouriteArticles(completion: @escaping ([Article]) -> ()) {
        let url: String = "http://78.140.223.188:7070/childcare/users/fav"
        var request = URLRequest(url:  NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("Bearer \(Singleton.shared.userData?.token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        AF.request(request).responseJSON { response in
            var articleList = [Article]()
            var categoryList = [Category]()
            switch response.result {
                case .success(let data):
                    let JSON = data as! NSDictionary
                    let data = JSON["data"]
                    if let array = data as? NSArray {
                        for obj in array {
                            if let dict = obj as? NSDictionary {
                                let id = dict.value(forKey: "id")
                                let topic = dict.value(forKey: "topic")
                                let text = dict.value(forKey: "text")
                                let iconUrl = dict.value(forKey: "iconUrl")
                                let createdDate = dict.value(forKey: "createdDate")
                                let catrgories = dict.value(forKey: "categoryList") as! NSArray
                                for category in catrgories {
                                    if let item = category as? NSDictionary {
                                        let categotyId = item.value(forKey: "id")
                                        let code = item.value(forKey: "code")
                                        let type = item.value(forKey: "type")
                                        let name = item.value(forKey: "name")
                                        let iconUrl = item.value(forKey: "iconUrl")
                                        let articleCategory = Category(id: categotyId as? Int ?? 0, code: code as? String ?? "", type: type as? String ?? "", name: name as? String ?? "", iconUrl: iconUrl as? String ?? "")
                                        categoryList.append(articleCategory)
                                    }
                                }
                                let article = Article(id: id as? Int ?? 0, topic: topic as! String, text: text as! String, categoryList: categoryList, iconUrl: iconUrl as? String ?? "", createdDate: createdDate as? String ?? "")
                                articleList.append(article)
                            }
                        }
                    }
                    completion(articleList)
                break
                case .failure(let error):
                    print(error)
                break
            }
        }.resume()
    }


    static func getAllDicts(completion: @escaping () -> ()) {
        APIManagerCategories.getCategories { category in }
                    
        getOrganizations { hospitals in }
                
        getCitiesName { citites in }
                
        APIManagerChild.getChildListByParent { childs in }
        
        completion()
    }

}
